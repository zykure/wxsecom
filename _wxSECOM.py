# -*- coding: utf-8 -*-

###########################################################################
## Python code generated with wxFormBuilder (version Oct 12 2011)
## http://www.wxformbuilder.org/
##
## PLEASE DO "NOT" EDIT THIS FILE!
###########################################################################

import wx
import wx.grid

Panel1 = 1000
Plaintext = 1001
BtnEncrypt = 1002
BtnDecrypt = 1003
Ciphertext = 1004

###########################################################################
## Class MainFrame
###########################################################################

class MainFrame ( wx.Frame ):

	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"wxSECOM", pos = wx.DefaultPosition, size = wx.Size( 680,580 ), style = wx.DEFAULT_FRAME_STYLE|wx.RESIZE_BORDER|wx.TAB_TRAVERSAL, name = u"wxSECOM" )

		self.SetSizeHintsSz( wx.Size( 680,580 ), wx.DefaultSize )

		self.statusBar = self.CreateStatusBar( 4, 0, wx.ID_ANY )
		bSizer0 = wx.BoxSizer( wx.VERTICAL )

		self.notebook = wx.Notebook( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.notebook.SetToolTipString( u"Encryption & Decryption tab.\nHere, the input and output of the cipher algorithm are shown. The master key is also set in this tab." )

		self.panel1 = wx.Panel( self.notebook, Panel1, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer1 = wx.BoxSizer( wx.VERTICAL )

		self.textCtrlPlaintext = wx.TextCtrl( self.panel1, Plaintext, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_MULTILINE|wx.TE_WORDWRAP )
		self.textCtrlPlaintext.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), 70, 90, 90, False, "Monospace" ) )
		self.textCtrlPlaintext.SetToolTipString( u"In encrypt mode, you can enter your plaintext here. Only characters contained in the used charset are accepted; other characters will be replaced by spaces. You can check out the allowed characters at the \"Checkerboard\" tab. While typing, all connected elements (e.g., the ciphertext field) are continously updated.\n\nIn decrypt mode, the decrypted plaintext will be put here (overwriting previous contents!)." )

		bSizer1.Add( self.textCtrlPlaintext, 1, wx.ALL|wx.EXPAND, 10 )

		bSizer11 = wx.BoxSizer( wx.HORIZONTAL )

		self.toggleBtnEncrypt = wx.ToggleButton( self.panel1, BtnEncrypt, u"Encrypt", wx.DefaultPosition, wx.Size( 100,-1 ), 0 )
		self.toggleBtnEncrypt.SetValue( True )
		self.toggleBtnEncrypt.SetToolTipString( u"This button sets the application mode to \"encrypt\".\nIn this mode, you can enter a text in the upper field, which will then be encrypted." )

		bSizer11.Add( self.toggleBtnEncrypt, 0, wx.ALL, 5 )


		bSizer11.AddSpacer( 20 )

		self.staticText11a = wx.StaticText( self.panel1, wx.ID_ANY, u"Key:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticText11a.Wrap( -1 )
		bSizer11.Add( self.staticText11a, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )

		self.textCtrlKey = wx.TextCtrl( self.panel1, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_PROCESS_ENTER )
		self.textCtrlKey.SetToolTipString( u"This field sets the master keyphrase used for encrypting and decrypting.\nOnly characters from this keyphrase are used to get the derived keys; the minimum required length is 20 characters. Please note that due to the special key derivation procedure not all changes to the key make a difference in the output." )

		bSizer11.Add( self.textCtrlKey, 1, wx.ALL|wx.EXPAND, 5 )

		self.buttonKeyEnter = wx.Button( self.panel1, wx.ID_ANY, u"Set!", wx.DefaultPosition, wx.Size( 50,-1 ), 0 )
		self.buttonKeyEnter.SetToolTipString( u"Click this button to apply the key entered on the left." )

		bSizer11.Add( self.buttonKeyEnter, 0, wx.ALL, 5 )


		bSizer11.AddSpacer( 20 )

		self.toggleBtnDecrypt = wx.ToggleButton( self.panel1, BtnDecrypt, u"Decrypt", wx.DefaultPosition, wx.Size( 100,-1 ), 0 )
		self.toggleBtnDecrypt.SetToolTipString( u"This button sets the application mode to \"decrypt\".\nIn this mode, you can enter a text in the lower field, which will then be decrypted." )

		bSizer11.Add( self.toggleBtnDecrypt, 0, wx.ALL, 5 )

		bSizer1.Add( bSizer11, 0, wx.EXPAND, 5 )

		self.textCtrlCiphertext = wx.TextCtrl( self.panel1, Ciphertext, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_MULTILINE|wx.TE_READONLY|wx.TE_WORDWRAP )
		self.textCtrlCiphertext.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), 70, 90, 90, False, "Monospace" ) )
		self.textCtrlCiphertext.SetToolTipString( u"In encrypt mode, the encrypted ciphertext will be put here (overwriting previous contents!).\n\nIn decrypt mode, you can enter your ciphertext here. Only the ten digits 0..9 are allowed; other characters will be ignored. That way it is possible to enter the ciphertext in 5-digit groups, which is a very common practice and helps to avoid typing errors. \nWhile typing, all connected elements (e.g., the plaintext field) are continously updated." )

		bSizer1.Add( self.textCtrlCiphertext, 1, wx.ALL|wx.EXPAND, 10 )

		self.panel1.SetSizer( bSizer1 )
		self.panel1.Layout()
		bSizer1.Fit( self.panel1 )
		self.notebook.AddPage( self.panel1, u"Encrypt && Decrypt", True )
		self.panel2 = wx.Panel( self.notebook, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		self.panel2.SetToolTipString( u"Key Setup tab.\nThe specific settings for key derivation can be changed here. Also the details of the key derivation process are shown." )

		bSizer2 = wx.BoxSizer( wx.VERTICAL )

		sbSizer21 = wx.StaticBoxSizer( wx.StaticBox( self.panel2, wx.ID_ANY, u" Derived Keys  " ), wx.VERTICAL )

		bSizer211 = wx.BoxSizer( wx.HORIZONTAL )

		self.staticText211a = wx.StaticText( self.panel2, wx.ID_ANY, u"Left / Right Key:", wx.DefaultPosition, wx.Size( 150,-1 ), 0 )
		self.staticText211a.Wrap( -1 )
		bSizer211.Add( self.staticText211a, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )

		self.textCtrlLeftKey = wx.TextCtrl( self.panel2, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY )
		self.textCtrlLeftKey.SetToolTipString( u"The Left Key is constructed by using the first ten characters of the keyphrase. These ten characters are then sorted alphabetically and get an index number assigned to each (starting with 0). These number then provide the key which is used futher on.\nExample: ABCDEABCDE → 1357924680" )

		bSizer211.Add( self.textCtrlLeftKey, 1, wx.ALL, 2 )

		self.staticText211b = wx.StaticText( self.panel2, wx.ID_ANY, u"+", wx.DefaultPosition, wx.Size( -1,-1 ), 0 )
		self.staticText211b.Wrap( -1 )
		bSizer211.Add( self.staticText211b, 0, wx.ALL, 5 )

		self.textCtrlRightKey = wx.TextCtrl( self.panel2, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY )
		self.textCtrlRightKey.SetToolTipString( u"The Right Key is constructed by using the second ten characters of the keyphrase. These ten characters are then sorted alphabetically and get an index number assigned to each (starting with 0). These number then provide the key which is used futher on.\nExample: EDCBAEDCBA → 9753108642" )

		bSizer211.Add( self.textCtrlRightKey, 1, wx.ALL, 2 )

		sbSizer21.Add( bSizer211, 0, wx.EXPAND, 5 )

		bSizer213 = wx.BoxSizer( wx.HORIZONTAL )

		self.staticText213a = wx.StaticText( self.panel2, wx.ID_ANY, u"Combined Key:", wx.DefaultPosition, wx.Size( 150,-1 ), 0 )
		self.staticText213a.Wrap( -1 )
		bSizer213.Add( self.staticText213a, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )


		bSizer213.AddSpacer( 0 )

		self.textCtrlCombinedKey = wx.TextCtrl( self.panel2, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY )
		self.textCtrlCombinedKey.SetToolTipString( u"The Combined Key is derived by summing up each digit of the Left and Right Key (modulo 10).\nExample: 1357924680 + 9753108642 → 0000022222" )

		bSizer213.Add( self.textCtrlCombinedKey, 2, wx.ALL, 2 )


		bSizer213.AddSpacer( 0 )

		sbSizer21.Add( bSizer213, 1, wx.EXPAND, 5 )

		self.staticline21a = wx.StaticLine( self.panel2, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL )
		sbSizer21.Add( self.staticline21a, 0, wx.ALL|wx.EXPAND, 5 )

		bSizer214 = wx.BoxSizer( wx.HORIZONTAL )

		self.staticText214a = wx.StaticText( self.panel2, wx.ID_ANY, u"Expanded Key:", wx.DefaultPosition, wx.Size( 150,-1 ), 0 )
		self.staticText214a.Wrap( -1 )
		bSizer214.Add( self.staticText214a, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )

		self.textCtrlExpandedKey = wx.TextCtrl( self.panel2, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY )
		self.textCtrlExpandedKey.SetToolTipString( u"The Expanded Key is derived from the Combined Key by the method of chain addition (a.k.a. Lagged Fibonacci Generator, LFG). This method increases the entropy, even if the source itself has very low entropy. By default, the length of the Extended Key is set to 50 digits. You can change this value by using the slider below.\n\nChain addition simply extends the key by adding the sum of the first two digits at the end, then the sum of the second and third digits, and so on.\nExample: 0000022222 → 0000246800..." )

		bSizer214.Add( self.textCtrlExpandedKey, 1, wx.ALL, 2 )

		sbSizer21.Add( bSizer214, 1, wx.EXPAND, 5 )

		bSizer215 = wx.BoxSizer( wx.HORIZONTAL )

		self.staticText215a = wx.StaticText( self.panel2, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 150,-1 ), 0 )
		self.staticText215a.Wrap( -1 )
		bSizer215.Add( self.staticText215a, 0, wx.LEFT|wx.RIGHT, 5 )

		self.staticText215b = wx.StaticText( self.panel2, wx.ID_ANY, u"LFG length:", wx.DefaultPosition, wx.Size( 100,-1 ), 0 )
		self.staticText215b.Wrap( -1 )
		bSizer215.Add( self.staticText215b, 0, wx.ALL, 2 )

		self.sliderChainAddLength = wx.Slider( self.panel2, wx.ID_ANY, 50, 40, 200, wx.DefaultPosition, wx.Size( -1,-1 ), wx.SL_HORIZONTAL )
		self.sliderChainAddLength.SetToolTipString( u"Use this slider to changed the length of the Expanded Key. By default, this is set to 50 digits. The minimum value depends on the minimum length of the transposition keys (which can be set below)." )

		bSizer215.Add( self.sliderChainAddLength, 1, wx.ALL, 2 )

		self.staticTextChainAddLength = wx.StaticText( self.panel2, wx.ID_ANY, u"0", wx.DefaultPosition, wx.Size( 50,-1 ), 0 )
		self.staticTextChainAddLength.Wrap( -1 )
		bSizer215.Add( self.staticTextChainAddLength, 0, wx.ALIGN_CENTER_VERTICAL|wx.LEFT|wx.RIGHT, 5 )

		sbSizer21.Add( bSizer215, 0, wx.EXPAND, 5 )

		bSizer2.Add( sbSizer21, 0, wx.ALL|wx.EXPAND, 5 )

		sbSizer22 = wx.StaticBoxSizer( wx.StaticBox( self.panel2, wx.ID_ANY, u" Checkerboard Key  " ), wx.VERTICAL )

		bSizer221 = wx.BoxSizer( wx.HORIZONTAL )

		self.staticText221a = wx.StaticText( self.panel2, wx.ID_ANY, u"Source Key:", wx.DefaultPosition, wx.Size( 150,-1 ), 0 )
		self.staticText221a.Wrap( -1 )
		bSizer221.Add( self.staticText221a, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )

		self.textCtrlCBSourceKey = wx.TextCtrl( self.panel2, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY )
		self.textCtrlCBSourceKey.SetToolTipString( u"The Checkerboard Source Key simply is the last ten-digit group of the Expanded Key." )

		bSizer221.Add( self.textCtrlCBSourceKey, 1, wx.ALL, 2 )

		sbSizer22.Add( bSizer221, 1, wx.EXPAND, 5 )

		self.staticline22 = wx.StaticLine( self.panel2, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL )
		sbSizer22.Add( self.staticline22, 0, wx.EXPAND |wx.ALL, 5 )

		bSizer222 = wx.BoxSizer( wx.HORIZONTAL )

		self.staticText222a = wx.StaticText( self.panel2, wx.ID_ANY, u"Final Key:", wx.DefaultPosition, wx.Size( 150,-1 ), 0 )
		self.staticText222a.Wrap( -1 )
		bSizer222.Add( self.staticText222a, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )

		self.textCtrlCBFinalKey = wx.TextCtrl( self.panel2, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY )
		self.textCtrlCBFinalKey.SetToolTipString( u"The Final Key for the 1st encryption step, the straddling checkerboard substitution, is derived from the Source Key by sorting the ten digits numerically (beginning with 1) and assigning an index to each digit. These indices then form the key.\nExample: 1234512345 → 1357924680" )

		bSizer222.Add( self.textCtrlCBFinalKey, 1, wx.ALL, 2 )

		sbSizer22.Add( bSizer222, 1, wx.EXPAND, 5 )

		bSizer2.Add( sbSizer22, 0, wx.ALL|wx.EXPAND, 5 )

		sbSizer23 = wx.StaticBoxSizer( wx.StaticBox( self.panel2, wx.ID_ANY, u" Transposition Keys  " ), wx.VERTICAL )

		bSizer231 = wx.BoxSizer( wx.HORIZONTAL )

		self.staticText231a = wx.StaticText( self.panel2, wx.ID_ANY, u"Master Key:", wx.DefaultPosition, wx.Size( 150,-1 ), 0 )
		self.staticText231a.Wrap( -1 )
		bSizer231.Add( self.staticText231a, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )

		self.textCtrlTMasterKey = wx.TextCtrl( self.panel2, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY )
		self.textCtrlTMasterKey.SetToolTipString( u"The Transposition Master Key is derived by summing up each digit of the Checkerboard Final Key and the Right Key (modulo 10)." )

		bSizer231.Add( self.textCtrlTMasterKey, 1, wx.ALL, 2 )

		sbSizer23.Add( bSizer231, 1, wx.EXPAND, 5 )

		bSizer232 = wx.BoxSizer( wx.HORIZONTAL )

		self.staticText232a = wx.StaticText( self.panel2, wx.ID_ANY, u"Source Key:", wx.DefaultPosition, wx.Size( 150,-1 ), 0 )
		self.staticText232a.Wrap( -1 )
		bSizer232.Add( self.staticText232a, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )

		self.textCtrlTSourceKey = wx.TextCtrl( self.panel2, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY )
		self.textCtrlTSourceKey.SetToolTipString( u"The Transposition Source Key is derived from the Master Key using a simple columnar transposition of width 10, using the Expanded Key as input and the Master Key for transposition.\nLook at the Transposition tab to get an idea of this transposition procedure. The Expanded Key is written in rows into the transposition table, and then read out in columns according to the order specified by the Master Key." )

		bSizer232.Add( self.textCtrlTSourceKey, 1, wx.ALL, 2 )

		sbSizer23.Add( bSizer232, 1, wx.EXPAND, 5 )

		self.staticline33 = wx.StaticLine( self.panel2, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL )
		sbSizer23.Add( self.staticline33, 0, wx.EXPAND |wx.ALL, 5 )

		bSizer233 = wx.BoxSizer( wx.HORIZONTAL )

		self.staticText233a = wx.StaticText( self.panel2, wx.ID_ANY, u"Final Keys:", wx.DefaultPosition, wx.Size( 150,-1 ), 0 )
		self.staticText233a.Wrap( -1 )
		bSizer233.Add( self.staticText233a, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )

		self.textCtrlT1Key = wx.TextCtrl( self.panel2, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY )
		self.textCtrlT1Key.SetToolTipString( u"The Final Key for the 2nd exncryption step, the columnar transposition, is derived from the Transposition Source simply using the last W digits, where W is the width of this transposition.\nThe width itself is computed from the Expanded Key, summing up all odd numbers beginning with the right end, until W is greater than the minimum length. The minimum length can be set with the slider below.\nExample: ...123234456 → W = 5 + 3 + 3 + ..." )

		bSizer233.Add( self.textCtrlT1Key, 1, wx.ALL, 2 )

		self.staticTextT1Width = wx.StaticText( self.panel2, wx.ID_ANY, u"0", wx.DefaultPosition, wx.Size( 25,-1 ), 0 )
		self.staticTextT1Width.Wrap( -1 )
		bSizer233.Add( self.staticTextT1Width, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL|wx.LEFT|wx.RIGHT, 2 )

		self.textCtrlT2Key = wx.TextCtrl( self.panel2, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY )
		self.textCtrlT2Key.SetToolTipString( u"The Final Key for the 3nd exncryption step, the disrupted, columnar transposition, is derived from the Transposition Source exactly like the 1st Final Key, only using the last W digits from the remaining Expanded Key (i.e. after reading out the 1st Final Key), where W is the width of this transposition." )

		bSizer233.Add( self.textCtrlT2Key, 1, wx.ALL, 2 )

		self.staticTextT2Width = wx.StaticText( self.panel2, wx.ID_ANY, u"0", wx.DefaultPosition, wx.Size( 25,-1 ), 0 )
		self.staticTextT2Width.Wrap( -1 )
		bSizer233.Add( self.staticTextT2Width, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 2 )

		sbSizer23.Add( bSizer233, 1, wx.EXPAND, 5 )

		bSizer234 = wx.BoxSizer( wx.HORIZONTAL )

		self.staticText234a = wx.StaticText( self.panel2, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 150,-1 ), 0 )
		self.staticText234a.Wrap( -1 )
		bSizer234.Add( self.staticText234a, 0, wx.LEFT|wx.RIGHT, 5 )

		self.staticText234b = wx.StaticText( self.panel2, wx.ID_ANY, u"Min. length:", wx.DefaultPosition, wx.Size( 100,-1 ), 0 )
		self.staticText234b.Wrap( -1 )
		bSizer234.Add( self.staticText234b, 0, wx.ALL, 2 )

		self.sliderTransMinWidth = wx.Slider( self.panel2, wx.ID_ANY, 10, 10, 25, wx.DefaultPosition, wx.Size( -1,-1 ), wx.SL_HORIZONTAL )
		self.sliderTransMinWidth.SetToolTipString( u"Use this slider to set the minimum length of both transpsoition keys, which in turn correspond to the widths of the transposition tables. By default, this value is set to 10. The Expanded Key's length is required to be at least four times this value to make sure that enough digits are there.\n(A minimum length of 10 can result in a transposition width of up to 18 columns, therefore we would need an Expanded Key of at least 2 × 18 = 36 digits!)" )

		bSizer234.Add( self.sliderTransMinWidth, 1, wx.ALL, 2 )

		self.staticTextTransMinWidth = wx.StaticText( self.panel2, wx.ID_ANY, u"0", wx.DefaultPosition, wx.Size( 50,-1 ), 0 )
		self.staticTextTransMinWidth.Wrap( -1 )
		bSizer234.Add( self.staticTextTransMinWidth, 0, wx.ALIGN_CENTER_VERTICAL|wx.LEFT|wx.RIGHT, 5 )

		sbSizer23.Add( bSizer234, 0, wx.EXPAND, 5 )

		bSizer2.Add( sbSizer23, 0, wx.ALL|wx.EXPAND, 5 )

		bSizer24 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText241 = wx.StaticText( self.panel2, wx.ID_ANY, u"Key entropy coloring:", wx.DefaultPosition, wx.Size( 150,-1 ), 0 )
		self.m_staticText241.Wrap( -1 )
		bSizer24.Add( self.m_staticText241, 0, wx.ALL, 5 )

		self.m_panel241 = wx.Panel( self.panel2, wx.ID_ANY, wx.DefaultPosition, wx.Size( -1,-1 ), wx.RAISED_BORDER )
		bSizer241 = wx.BoxSizer( wx.VERTICAL )

		self.m_staticText241a = wx.StaticText( self.m_panel241, wx.ID_ANY, u"< 75%", wx.DefaultPosition, wx.Size( -1,-1 ), wx.ALIGN_CENTRE|wx.RAISED_BORDER )
		self.m_staticText241a.Wrap( -1 )
		bSizer241.Add( self.m_staticText241a, 0, wx.ALIGN_CENTER|wx.ALL, 5 )

		self.m_panel241.SetSizer( bSizer241 )
		self.m_panel241.Layout()
		bSizer241.Fit( self.m_panel241 )
		bSizer24.Add( self.m_panel241, 1, wx.LEFT|wx.RIGHT, 5 )

		self.m_panel242 = wx.Panel( self.panel2, wx.ID_ANY, wx.DefaultPosition, wx.Size( -1,-1 ), wx.RAISED_BORDER )
		bSizer242 = wx.BoxSizer( wx.VERTICAL )

		self.m_staticText242a = wx.StaticText( self.m_panel242, wx.ID_ANY, u"75 - 90 %", wx.DefaultPosition, wx.Size( -1,-1 ), wx.ALIGN_CENTRE|wx.RAISED_BORDER )
		self.m_staticText242a.Wrap( -1 )
		bSizer242.Add( self.m_staticText242a, 1, wx.ALIGN_CENTER|wx.ALL, 5 )

		self.m_panel242.SetSizer( bSizer242 )
		self.m_panel242.Layout()
		bSizer242.Fit( self.m_panel242 )
		bSizer24.Add( self.m_panel242, 1, wx.LEFT|wx.RIGHT, 5 )

		self.m_panel243 = wx.Panel( self.panel2, wx.ID_ANY, wx.DefaultPosition, wx.Size( -1,-1 ), wx.RAISED_BORDER )
		bSizer243 = wx.BoxSizer( wx.VERTICAL )

		self.m_staticText243a = wx.StaticText( self.m_panel243, wx.ID_ANY, u"90 - 95 %", wx.DefaultPosition, wx.Size( -1,-1 ), wx.ALIGN_CENTRE|wx.RAISED_BORDER )
		self.m_staticText243a.Wrap( -1 )
		bSizer243.Add( self.m_staticText243a, 1, wx.ALIGN_CENTER|wx.ALL, 5 )

		self.m_panel243.SetSizer( bSizer243 )
		self.m_panel243.Layout()
		bSizer243.Fit( self.m_panel243 )
		bSizer24.Add( self.m_panel243, 1, wx.LEFT|wx.RIGHT, 5 )

		self.m_panel244 = wx.Panel( self.panel2, wx.ID_ANY, wx.DefaultPosition, wx.Size( 50,-1 ), wx.RAISED_BORDER )
		bSizer244 = wx.BoxSizer( wx.VERTICAL )

		self.m_staticText244a = wx.StaticText( self.m_panel244, wx.ID_ANY, u"95 - 99.99 %", wx.DefaultPosition, wx.Size( -1,-1 ), wx.ALIGN_CENTRE|wx.RAISED_BORDER )
		self.m_staticText244a.Wrap( -1 )
		bSizer244.Add( self.m_staticText244a, 1, wx.ALIGN_CENTER|wx.ALL, 5 )

		self.m_panel244.SetSizer( bSizer244 )
		self.m_panel244.Layout()
		bSizer24.Add( self.m_panel244, 1, wx.LEFT|wx.RIGHT, 5 )

		self.m_panel245 = wx.Panel( self.panel2, wx.ID_ANY, wx.DefaultPosition, wx.Size( -1,-1 ), wx.RAISED_BORDER )
		bSizer245 = wx.BoxSizer( wx.VERTICAL )

		self.m_staticText245a = wx.StaticText( self.m_panel245, wx.ID_ANY, u"> 99.99 %", wx.DefaultPosition, wx.Size( -1,-1 ), wx.ALIGN_CENTRE|wx.RAISED_BORDER )
		self.m_staticText245a.Wrap( -1 )
		bSizer245.Add( self.m_staticText245a, 1, wx.ALIGN_CENTER|wx.ALL, 5 )

		self.m_panel245.SetSizer( bSizer245 )
		self.m_panel245.Layout()
		bSizer245.Fit( self.m_panel245 )
		bSizer24.Add( self.m_panel245, 1, wx.LEFT|wx.RIGHT, 5 )

		bSizer2.Add( bSizer24, 1, wx.ALL|wx.EXPAND, 10 )

		self.panel2.SetSizer( bSizer2 )
		self.panel2.Layout()
		bSizer2.Fit( self.panel2 )
		self.notebook.AddPage( self.panel2, u"Key Setup", False )
		self.panel3 = wx.Panel( self.notebook, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		self.panel3.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), 70, 90, 90, False, "Monospace" ) )
		self.panel3.SetToolTipString( u"Straddled Checkerboard tab.\nThe Checkerboard defines the first encryption step - a monoalphabetic, straddled substitution. The character set can be changed here, too." )

		bSizer3 = wx.BoxSizer( wx.VERTICAL )

		bSizer31 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText2 = wx.StaticText( self.panel3, wx.ID_ANY, u"Character set preset:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText2.Wrap( -1 )
		bSizer31.Add( self.m_staticText2, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )

		comboBoxCharacterSetChoices = []
		self.comboBoxCharacterSet = wx.ComboBox( self.panel3, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, comboBoxCharacterSetChoices, wx.CB_DROPDOWN )
		self.comboBoxCharacterSet.SetFont( wx.Font( 10, 76, 90, 92, False, "Monospace" ) )
		self.comboBoxCharacterSet.SetToolTipString( u"You can choose one of many variants of checkerboard charsets. With this four-row checkerboard, we can encrypt 37 characters (4 × 10 minus the empty cells in the first row). Since characters in the first row will be encrypted using only one digit, it is indeed useful to put high-frequency characters here.\nFor the english language, the seven most high-frequent letters are E, T, A, O, I, N, S (in this order). These letters make up about 58 percent of a normal, modern-english text (counting only letters, not spaces or numbers).\nFor the german language, the seven characters are I, N, S, E, R, A, T.\n\nIf one would need more than 37 characters to encrypt, the checkerboard could be extended to five rows, using six characters in the top row, giving a set of 46 characters. On the other hand, the checkerboard could be reduced to three rows, giving a set of 28 characters. The famous \"A SIN TO ER(R)\" phrase comes from this variant, using the characters ASINTOER in the top row and leaving two cells empty. Russian agents often used \"SNEGOPA\" (СНЕГОПА) in the top row, the russian word for snow.\nThe character \"/\" was often used as a numeric escape character for character sets that don't contain numbers, e.g. using /ABCDEFGHI/ as replacement for 1234567890." )

		bSizer31.Add( self.comboBoxCharacterSet, 1, wx.ALL|wx.EXPAND, 5 )

		bSizer3.Add( bSizer31, 0, wx.EXPAND, 5 )

		self.staticline31a = wx.StaticLine( self.panel3, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL )
		bSizer3.Add( self.staticline31a, 0, wx.EXPAND |wx.ALL, 5 )

		bSizer3a1 = wx.BoxSizer( wx.VERTICAL )

		self.gridCheckerboard = wx.grid.Grid( self.panel3, wx.ID_ANY, wx.DefaultPosition, wx.Size( -1,-1 ), wx.HSCROLL|wx.SUNKEN_BORDER|wx.VSCROLL )

		# Grid
		self.gridCheckerboard.CreateGrid( 4, 10 )
		self.gridCheckerboard.EnableEditing( False )
		self.gridCheckerboard.EnableGridLines( False )
		self.gridCheckerboard.EnableDragGridSize( False )
		self.gridCheckerboard.SetMargins( 0, 0 )

		# Columns
		self.gridCheckerboard.AutoSizeColumns()
		self.gridCheckerboard.EnableDragColMove( False )
		self.gridCheckerboard.EnableDragColSize( False )
		self.gridCheckerboard.SetColLabelSize( 25 )
		self.gridCheckerboard.SetColLabelAlignment( wx.ALIGN_CENTRE, wx.ALIGN_CENTRE )

		# Rows
		self.gridCheckerboard.EnableDragRowSize( True )
		self.gridCheckerboard.SetRowLabelSize( 50 )
		self.gridCheckerboard.SetRowLabelAlignment( wx.ALIGN_CENTRE, wx.ALIGN_CENTRE )

		# Label Appearance
		self.gridCheckerboard.SetLabelBackgroundColour( wx.Colour( 239, 239, 239 ) )
		self.gridCheckerboard.SetLabelTextColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_BTNTEXT ) )

		# Cell Defaults
		self.gridCheckerboard.SetDefaultCellAlignment( wx.ALIGN_CENTRE, wx.ALIGN_CENTRE )
		self.gridCheckerboard.SetToolTipString( u"This table shows the checkerboard used for the 1st encryption step.\nThe columns are labeled with the ten Checkerboard Key digits. The cells are then filled with the characters from the selected character set. The row labels match the columns where an empty cell sits in the first row.\nTo encrypt a character, it is looked up in the table, and the corresponding row and column numbers are used for substitution. If the character is in the top row, only one digit is used for encryption (the column number). For the other rows, two digits are used, i.e. row number + column number. (This also explains why the row numbers are constructed like described above.)" )

		bSizer3a1.Add( self.gridCheckerboard, 1, wx.ALL|wx.EXPAND, 10 )

		bSizer3.Add( bSizer3a1, 1, wx.EXPAND, 5 )


		bSizer3.AddSpacer( 5 )

		sbSizer32 = wx.StaticBoxSizer( wx.StaticBox( self.panel3, wx.ID_ANY, u" Test Encryption  " ), wx.VERTICAL )

		bSizer321 = wx.BoxSizer( wx.HORIZONTAL )

		self.textCtrlCheckerboardTestInput = wx.TextCtrl( self.panel3, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.textCtrlCheckerboardTestInput.SetToolTipString( u"You can enter some text here to quickly test the checkerboard subsitution, using the checkerboard table displayed above." )

		bSizer321.Add( self.textCtrlCheckerboardTestInput, 1, wx.ALL, 5 )

		self.staticTextCheckerboardTestInputLength = wx.StaticText( self.panel3, wx.ID_ANY, u"0", wx.DefaultPosition, wx.Size( 25,-1 ), 0 )
		self.staticTextCheckerboardTestInputLength.Wrap( -1 )
		bSizer321.Add( self.staticTextCheckerboardTestInputLength, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )

		self.m_staticText321a = wx.StaticText( self.panel3, wx.ID_ANY, u"→", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText321a.Wrap( -1 )
		bSizer321.Add( self.m_staticText321a, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )

		self.textCtrlCheckerboardTestOutput = wx.TextCtrl( self.panel3, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY )
		self.textCtrlCheckerboardTestOutput.SetToolTipString( u"This field shows the result of applying checkerboard encryption to the text on the left. Also note the increase in text length displayed next to the field, which is kept rather small due to the straddling checkerboard substitution used." )

		bSizer321.Add( self.textCtrlCheckerboardTestOutput, 1, wx.ALL, 5 )

		self.staticTextCheckerboardTestOutputLength = wx.StaticText( self.panel3, wx.ID_ANY, u"0", wx.DefaultPosition, wx.Size( 25,-1 ), 0 )
		self.staticTextCheckerboardTestOutputLength.Wrap( -1 )
		bSizer321.Add( self.staticTextCheckerboardTestOutputLength, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )

		sbSizer32.Add( bSizer321, 0, wx.EXPAND, 5 )

		bSizer3.Add( sbSizer32, 0, wx.EXPAND, 5 )

		self.panel3.SetSizer( bSizer3 )
		self.panel3.Layout()
		bSizer3.Fit( self.panel3 )
		self.notebook.AddPage( self.panel3, u"Checkerboard", False )
		self.panel4 = wx.Panel( self.notebook, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL|wx.VSCROLL )
		self.panel4.SetToolTipString( u"Transposition tab.\nThe table of the first transposition is shown in this tab." )

		bSizer4 = wx.BoxSizer( wx.VERTICAL )

		self.gridTransp1 = wx.grid.Grid( self.panel4, wx.ID_ANY, wx.DefaultPosition, wx.Size( -1,-1 ), wx.HSCROLL|wx.SUNKEN_BORDER|wx.VSCROLL )

		# Grid
		self.gridTransp1.CreateGrid( 0, 10 )
		self.gridTransp1.EnableEditing( False )
		self.gridTransp1.EnableGridLines( False )
		self.gridTransp1.EnableDragGridSize( False )
		self.gridTransp1.SetMargins( 0, 0 )

		# Columns
		self.gridTransp1.AutoSizeColumns()
		self.gridTransp1.EnableDragColMove( False )
		self.gridTransp1.EnableDragColSize( False )
		self.gridTransp1.SetColLabelSize( 25 )
		self.gridTransp1.SetColLabelAlignment( wx.ALIGN_CENTRE, wx.ALIGN_CENTRE )

		# Rows
		self.gridTransp1.EnableDragRowSize( True )
		self.gridTransp1.SetRowLabelSize( 0 )
		self.gridTransp1.SetRowLabelAlignment( wx.ALIGN_CENTRE, wx.ALIGN_CENTRE )

		# Label Appearance
		self.gridTransp1.SetLabelBackgroundColour( wx.Colour( 240, 240, 240 ) )
		self.gridTransp1.SetLabelTextColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_BTNTEXT ) )

		# Cell Defaults
		self.gridTransp1.SetDefaultCellAlignment( wx.ALIGN_CENTRE, wx.ALIGN_CENTRE )
		self.gridTransp1.SetToolTipString( u"This table shows the transposition used for the 2nd encryption step.\nThe table is just fille with the output of the 1st encryption step, the monoalphabetic substitution. The filling is done line by line, while the width of the transposition equals the length of the 1st transposition key. You can enlarge the width by setting a higher minimum transposition width in the Key tab.\nThe columns are labeled after the 1st transposition key and are read out in the order defined by that key to get the output of this encryption step.\nNote that the columns are ordered after the decimal digits of the key, with 1 being the lowest number and 0 the highest. If one digit appears in the key more than once, the leftmost digit is used first, and so on." )

		bSizer4.Add( self.gridTransp1, 1, wx.ALL|wx.EXPAND, 10 )

		self.panel4.SetSizer( bSizer4 )
		self.panel4.Layout()
		bSizer4.Fit( self.panel4 )
		self.notebook.AddPage( self.panel4, u"Transposition", False )
		self.panel5 = wx.Panel( self.notebook, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL|wx.VSCROLL )
		self.panel5.SetToolTipString( u"Disrupted Transposition tab.\nThe table of the second transposition is shown in this tab." )

		bSizer51 = wx.BoxSizer( wx.VERTICAL )

		self.gridTransp2 = wx.grid.Grid( self.panel5, wx.ID_ANY, wx.DefaultPosition, wx.Size( -1,-1 ), wx.HSCROLL|wx.SUNKEN_BORDER|wx.VSCROLL )

		# Grid
		self.gridTransp2.CreateGrid( 0, 10 )
		self.gridTransp2.EnableEditing( False )
		self.gridTransp2.EnableGridLines( False )
		self.gridTransp2.EnableDragGridSize( False )
		self.gridTransp2.SetMargins( 0, 0 )

		# Columns
		self.gridTransp2.AutoSizeColumns()
		self.gridTransp2.EnableDragColMove( False )
		self.gridTransp2.EnableDragColSize( False )
		self.gridTransp2.SetColLabelSize( 25 )
		self.gridTransp2.SetColLabelAlignment( wx.ALIGN_CENTRE, wx.ALIGN_CENTRE )

		# Rows
		self.gridTransp2.EnableDragRowSize( True )
		self.gridTransp2.SetRowLabelSize( 0 )
		self.gridTransp2.SetRowLabelAlignment( wx.ALIGN_CENTRE, wx.ALIGN_CENTRE )

		# Label Appearance
		self.gridTransp2.SetLabelBackgroundColour( wx.Colour( 240, 240, 240 ) )
		self.gridTransp2.SetLabelTextColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_BTNTEXT ) )

		# Cell Defaults
		self.gridTransp2.SetDefaultCellAlignment( wx.ALIGN_CENTRE, wx.ALIGN_CENTRE )
		self.gridTransp2.SetToolTipString( u"This table shows the disrupted transposition used for the 3rd encryption step.\nThe rules for the first transposition also apply here, with the difference that the lines are not filled directly with the output of the 2nd encryption step. (It is also important to know the number of rows in the table before starting to fill up the table. This can be easily computed by H = N / W, where N is the number of digits and W is the transposition width. To get the number of columns to be used in the last row (see below for details), use X = N mod W.)\nInstead, the 2nd transposition key is used to get a disrupted transposition. This means that the first line is not filled completely at first, but only up to the column which corresponds to the lowest keys digit (using the rules described at the first transposition table). Then, the next line is filled up like that, but moving the last column one to the right. This is repeated until the rightmost columns is reached, after which a completely filled line follows. The line after that is then filled again, up to the column corresponding to the second key digit, and so on. This scheme is used until the previously computed number of rows is reached, leaving one part of the last row empty if appropriate.\nIn the second step, the still empty parts of the table are filled with the remaining digits. The table is then read out using the order defined by the transposition key, as described at the first transposition table, to finally generate the SECOM output." )

		bSizer51.Add( self.gridTransp2, 1, wx.ALL|wx.EXPAND, 10 )

		self.panel5.SetSizer( bSizer51 )
		self.panel5.Layout()
		bSizer51.Fit( self.panel5 )
		self.notebook.AddPage( self.panel5, u"Disrupted Transposition", False )

		bSizer0.Add( self.notebook, 1, wx.ALL|wx.EXPAND, 5 )

		self.SetSizer( bSizer0 )
		self.Layout()

		self.Centre( wx.BOTH )

		# Connect Events
		self.panel1.Bind( wx.EVT_ENTER_WINDOW, self.setFocus )
		self.textCtrlPlaintext.Bind( wx.EVT_KILL_FOCUS, self.updatePlaintext )
		self.textCtrlPlaintext.Bind( wx.EVT_TEXT, self.updateEncryption )
		self.toggleBtnEncrypt.Bind( wx.EVT_LEAVE_WINDOW, self.updatePlaintext )
		self.toggleBtnEncrypt.Bind( wx.EVT_TOGGLEBUTTON, self.toggleEncDec )
		self.textCtrlKey.Bind( wx.EVT_TEXT, self.checkKeyInput )
		self.textCtrlKey.Bind( wx.EVT_TEXT_ENTER, self.updateKeys )
		self.buttonKeyEnter.Bind( wx.EVT_BUTTON, self.updateKeys )
		self.toggleBtnDecrypt.Bind( wx.EVT_TOGGLEBUTTON, self.toggleEncDec )
		self.textCtrlCiphertext.Bind( wx.EVT_TEXT, self.updateDecryption )
		self.textCtrlLeftKey.Bind( wx.EVT_TEXT, self.updateCombinedKey )
		self.textCtrlRightKey.Bind( wx.EVT_TEXT, self.updateCombinedKey )
		self.textCtrlCombinedKey.Bind( wx.EVT_TEXT, self.updateExpandedKey )
		self.textCtrlExpandedKey.Bind( wx.EVT_TEXT, self.updateCBSourceKey )
		self.sliderChainAddLength.Bind( wx.EVT_SCROLL, self.updateExpandedKey )
		self.textCtrlCBSourceKey.Bind( wx.EVT_TEXT, self.updateCBFinalKey )
		self.textCtrlCBFinalKey.Bind( wx.EVT_TEXT, self.updateTranspMasterKey )
		self.textCtrlTMasterKey.Bind( wx.EVT_TEXT, self.updateTranspSourceKey )
		self.textCtrlTSourceKey.Bind( wx.EVT_TEXT, self.updateTranspKeys )
		self.textCtrlT1Key.Bind( wx.EVT_TEXT, self.updateTransp1 )
		self.textCtrlT2Key.Bind( wx.EVT_TEXT, self.updateTransp2 )
		self.sliderTransMinWidth.Bind( wx.EVT_SCROLL, self.updateTranspMinWidth )
		self.comboBoxCharacterSet.Bind( wx.EVT_COMBOBOX, self.updateCheckerboard )
		self.comboBoxCharacterSet.Bind( wx.EVT_TEXT_ENTER, self.updateCheckerboard )
		self.textCtrlCheckerboardTestInput.Bind( wx.EVT_TEXT, self.updateCheckerboardTest )

	def __del__( self ):
		pass


	# Virtual event handlers, overide them in your derived class
	def setFocus( self, event ):
		event.Skip()

	def updatePlaintext( self, event ):
		event.Skip()

	def updateEncryption( self, event ):
		event.Skip()


	def toggleEncDec( self, event ):
		event.Skip()

	def checkKeyInput( self, event ):
		event.Skip()

	def updateKeys( self, event ):
		event.Skip()



	def updateDecryption( self, event ):
		event.Skip()

	def updateCombinedKey( self, event ):
		event.Skip()


	def updateExpandedKey( self, event ):
		event.Skip()

	def updateCBSourceKey( self, event ):
		event.Skip()


	def updateCBFinalKey( self, event ):
		event.Skip()

	def updateTranspMasterKey( self, event ):
		event.Skip()

	def updateTranspSourceKey( self, event ):
		event.Skip()

	def updateTranspKeys( self, event ):
		event.Skip()

	def updateTransp1( self, event ):
		event.Skip()

	def updateTransp2( self, event ):
		event.Skip()

	def updateTranspMinWidth( self, event ):
		event.Skip()

	def updateCheckerboard( self, event ):
		event.Skip()


	def updateCheckerboardTest( self, event ):
		event.Skip()
