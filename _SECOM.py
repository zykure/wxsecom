#!/usr/bin/env python
# -*- coding:utf8 -*-
#
#
# wxSECOM - GUI-implementation of the SECOM cipher
#
# File:		_SECOM.py
# Desc:		The core of the SECOM cipher.
#			Contains four classes: Key, CheckerBoard, TranspositionTable and
#           DisruptedTranspositionTable plus some helper functions.
#           If this program is run directly from the command line, some kind
#           of self-test is performed.
#
# Version:	2010 Nov 01
#
# Usage: python wxSECOM.py
#
# Copyright (c) 2010 Jan D. Behrens, <zykure@web.de>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA


import math

class Key:
    _key = []

    def __init__(self, key=[]):
        self._key = key[:]

    def __str__(self):
        return "".join([ str(x) for x in self._key ])

    def __len__(self):
        return len(self._key)

    def __getitem__(self, k):
        return self._key[k]

    def __setitem__(self, k, v):
        self._key[k] = v

    def append(self, l):
        self._key.append(l)

    def add(self, other):
        assert(len(self) == len(other))
        for i in range(len(self)):
            self[i] = (self[i] + other[i]) % 10

    def fromString(self, keyphrase):
        self._key = []
        for x in keyphrase:
            if x != " ":
                self.append( x.upper() )

    def subKey(self, start, length):
        return Key(self[start:start+length])


    def reduceAlpha(self):
        assert(len(self) == 10)
        result = [ 0 for x in range(10) ]

        n = 1
        for char in range(65,91):       # characters A .. Z
            for i in range(10):
                if ord(self[i]) == char:
                    result[i] = n
                    n = (n + 1) % 10

        self._key = result

    def reduceNum(self):
        assert(len(self) == 10)
        result = [ 0 for x in range(10) ]

        n = 1
        for num in range(1,11):         # numbers 1 .. 10=0
            for i in range(10):
                if self[i] == num % 10:
                    result[i] = n
                    n = (n + 1) % 10

        self._key = result

    def chainAdd(self, length):
        for i in range(length):
            self.append( (self[i] + self[i+1]) % 10 )

    def getEntropy(self, precision=-1):
        length = len(self._key)
        hist = [ 0 for x in range(10) ]
        for x in self._key:
            hist[int(x)] += 1
        entropy = 0.0;
        for h in hist:
            p = float(h) / float(length)
            if p > 0.0:
                entropy -= p * math.log10(p)

        if precision < 0:
            return entropy
        else:
            return "E = %.*f" % (precision, entropy)


class CheckerBoard:
    _charset = ""
    _key = ""
    _rows = []
    _rowIndex = []
    _forwardIndex = {}
    _backwardIndex = {}

    def __init__(self, key, charset="ES_TO_NI_ABCDFGHJKLMPQRUVWXYZ 1234567890"):
        self._key = key
        self._charset = charset.upper()
        self.buildCheckerboard()

    def __str__(self):
        result = ""
        result += "     %s" % " ".join([ str(x) for x in self._key ])
        result += "\n     " + "-"*(2*len(self._key)-1)
        result += "\n   | %s" % " ".join([ str(x) for x in self._rows[0] ])
        for r in range(0,len(self._rowIndex)):
            result += "\n%2d | %s" % (self._rowIndex[r], " ".join([ str(x) for x in self._rows[r+1] ]))
        return result

    def getBoard(self):
        result = []
        result.append( (-1, self._rows[0]) )
        for i in range(1,len(self._rows)):
            result.append( (self._rowIndex[i-1], self._rows[i]) )
        return result

    def buildCheckerboard(self):
        assert(len(self._charset) % 10 == 0)
        self._rows = [ self._charset[0:10] ]
        self._rowIndex = []
        for i in range(10):
            if self._rows[0][i] == "_":
                self._rowIndex.append(self._key[i])

        for k in range(1, len(self._rowIndex)+1):
            p = 10*(k+1) - self._rowIndex[k-1] + 1
            self._rows.append( self._charset[p:10*(k+1)] + self._charset[10*k:p] )

        self._forwardIndex = {}
        # set up first row
        for i in range(10):
            if self._rows[0][i] != " ":
                 self._forwardIndex[self._rows[0][i]] = self._key[i]
        # set up remaining rows
        for r in range(1,len(self._rows)):
            for i in range(10):
                 self._forwardIndex[self._rows[r][i]] = 10*self._rowIndex[r-1] + self._key[i]

        for k,v in  self._forwardIndex.items():
             self._backwardIndex[v] = k

    def encrypt(self, plain):
        cipher = []
        for x in plain.upper():
            cipher.append(self._forwardIndex[x])

        return "".join( [ str(x) for x in cipher ] )

    def decrypt(self, cipher):
        plain = []
        pos = 0
        while pos < len(cipher):
            if int(cipher[pos]) in self._rowIndex:
                if pos+1 >= len(cipher):
                    break
                x = cipher[pos] + cipher[pos+1]
                pos += 2
            else:
                x = cipher[pos]
                pos += 1
            plain.append(self._backwardIndex[int(x)])

        return "".join( [ x for x in plain ] )


class TranspositionTable:
    _width = 0
    _height = 0
    _length = 0
    _key = []
    _orderIndex = []
    _table = []

    def __init__(self, key):
        self._key = key
        self._width = len(key)

        self._orderIndex = []
        for num in range(1,11):  # numbers 1..10=0
            for k in range(self._width):
                if self._key[k] == num % 10:
                    self._orderIndex.append(k)

    def __str__(self):
        result = ""
        result += " %s" % " ".join([ str(x) for x in self._key ])
        if self._height > 0:
            result += "\n " + "-"*(2*self._width-1)
        for r in range(self._height):
            result += "\n %s" % " ".join([ str(x) for x in self._table[r] ])
        return result

    def __len__(self):
        return self._length

    def getRows(self):
        return self._table[:]

    def loadPlain(self, plain):
        self._length = len(plain)
        self._height = int(self._length / self._width)
        if (self._length % self._width) != 0:
            self._height += 1
        self._table = [ [] for x in range(self._height) ]

        pos = 0
        for r in range(self._height):
            self._table[r].extend(plain[pos:pos+self._width])
            pos += self._width

    def readPlain(self, getString=True):
        plain = []
        for r in range(self._height):
            if r == self._height-1:
                plain.extend(self._table[r][:self._length%self._width])
            else:
                plain.extend(self._table[r])

        if getString:
            return "".join( [ str(x) for x in plain ] )
        else:
            return plain

    def loadCipher(self, cipher):
        self._length = len(cipher)
        self._height = int(self._length / self._width)
        if (self._length % self._width) != 0:
            self._height += 1
        self._table = [ [ None for y in range(self._width)] for x in range(self._height) ]

        pos = 0
        for k in self._orderIndex:
            for r in range(self._height):
                if r == self._height-1:
                    if k >= self._length % self._width:
                        continue
                self._table[r][k] = cipher[pos]
                pos += 1
                if pos >= len(cipher):
                    break

    def readCipher(self, getString=True):
        cipher = []
        for k in self._orderIndex:
            for r in range(self._height):
                if k < len(self._table[r]):
                    cipher.append(self._table[r][k])

        if getString:
            return "".join( [ str(x) for x in cipher ] )
        else:
            return cipher


class DisruptedTranspositionTable(TranspositionTable):
    def getRows(self):
        result = []
        order = 0
        rowLen = self._width
        for r in range(self._height):
            if rowLen == self._width:
                rowLen = self._orderIndex[order]
                order += 1
                if order >= self._width:
                    order = 0
            else:
                rowLen += 1

            result.append( (rowLen, self._table[r]) )

        return result

    def loadPlain(self, plain):
        self._length = len(plain)
        self._height = int(self._length / self._width)
        if (self._length % self._width) != 0:
            self._height += 1
        self._table = [ [] for x in range(self._height) ]

        pos = 0
        order = 0
        rowLen = self._width
        for r in range(self._height):
            if rowLen == self._width:
                rowLen = self._orderIndex[order]
                order += 1
                if order >= self._width:
                    order = 0
            else:
                rowLen += 1

            if r == self._height-1:
                rowLen = (self._length % self._width)

            self._table[r].extend(plain[pos:pos+rowLen])
            pos += rowLen

        order = 0
        rowLen = self._width
        for r in range(self._height):
            if rowLen == self._width:
                rowLen = self._orderIndex[order]
                order += 1
                if order >= self._width:
                    order = 0
            else:
                rowLen += 1

            self._table[r].extend(plain[pos:pos+self._width-rowLen])
            pos += self._width - rowLen

    def readPlain(self, getString=True):
        plain = []
        order = 0
        rowLen = self._width
        for r in range(self._height):
            if rowLen == self._width:
                rowLen = self._orderIndex[order]
                order += 1
                if order >= self._width:
                    order = 0
            else:
                rowLen += 1

            if r == self._height-1:
                rowLen = (self._length % self._width)

            plain.extend(self._table[r][:rowLen])

        order = 0
        rowLen = self._width
        for r in range(self._height-1):
            if rowLen == self._width:
                rowLen = self._orderIndex[order]
                order += 1
                if order >= self._width:
                    order = 0
            else:
                rowLen += 1

            plain.extend(self._table[r][rowLen:])

        if getString:
            return "".join( [ str(x) for x in plain ] )
        else:
            return plain


def getTranspositionDimensions(key, minWidth=10):
    tableWidth1, tableWidth2 = 0, 0
    for i in range(len(key)-1,0,-1):
        if i < len(key)-1:
            if key[i] == key[i+1]:
                continue
        if tableWidth1 < minWidth:
            tableWidth1 += key[i]
        elif  tableWidth2 < minWidth:
            tableWidth2 += key[i]
        else:
            break

    return ( tableWidth1, tableWidth2 )

def printCipherText(cipher, blocksPerLine=6):
    if len(cipher) < 5:
        return cipher

    result = ""
    for i in range(int(len(cipher)/5)):
        result += cipher[5*i:5*i+5] + " "
        if blocksPerLine > 0 and (i+1) % blocksPerLine == 0:
            result += "\n"
    if len(cipher) > 5*(i+1):
        result += cipher[5*(i+1):]
    return result

if __name__ == "__main__":

    keyphrase = "MAKE NEW FRIENDS BUT KEEP THE OLD"
    plaintext = "RV TOMORROW AT 1400PM TO COMPLETE TRANSACTION USE DEADDROP AS USUALO"

    Keyphrase = Key()
    Keyphrase.fromString(keyphrase)
    assert(len(Keyphrase) >= 20)
    print "Keyphrase:", Keyphrase

    leftKey = Keyphrase.subKey(0,10)
    leftKey.reduceAlpha()
    rightKey = Keyphrase.subKey(10,10)
    rightKey.reduceAlpha()
    print "left Key: ", leftKey
    print "right Key:", rightKey

    expKey = Key(leftKey)
    expKey.add(rightKey)
    print "key:", expKey

    expKey.chainAdd(50)
    expKey = expKey.subKey(10, 60)
    print "expanded key:", expKey

    cbKey = expKey.subKey(50,10)
    cbKey.reduceNum()
    print "final key:", cbKey

    CB = CheckerBoard(cbKey)
    print "ckeckerboard:\n", CB

    ciph1 = CB.encrypt(plaintext)
    print "1st ciphertext:", ciph1, len(ciph1)

    plain1 = CB.decrypt(ciph1)
    print "-- restored plaintext:", plain1
    assert(plain1==plaintext)

    w1, w2 = getTranspositionDimensions(expKey)
    print "transposition widths:", w1, "/", w2

    tKey = Key(rightKey)
    tKey.add(cbKey)
    print "transposition master key:", tKey

    tKeyTab = TranspositionTable(tKey)
    tKeyTab.loadPlain(expKey)
    print "key transposition table:\n", tKeyTab

    KX = Key(tKeyTab.readCipher(getString=False))
    KX1 = KX.subKey(0, w1)
    KX2 = KX.subKey(w1, w2)
    print "transposition keys:", KX1, "/", KX2

    t1Tab = TranspositionTable(KX1)
    t1Tab.loadPlain(ciph1)
    print "1st transposition table:\n", t1Tab

    ciph2 = t1Tab.readCipher()
    print "2nd ciphertext:", ciph2, len(ciph2)

    XTab = TranspositionTable(KX1)
    XTab.loadCipher(ciph2)
    plain2 = XTab.readPlain()
    print "-- restored plaintext:", plain2
    assert(plain2==ciph1)

    t2Tab = DisruptedTranspositionTable(KX2)
    t2Tab.loadPlain(ciph2)
    print "3rd transposition table:\n", t2Tab

    ciph3 = t2Tab.readCipher()
    print "3rd ciphertext:", ciph3, len(ciph3)

    XTab = DisruptedTranspositionTable(KX2)
    XTab.loadCipher(ciph3)
    plain3 = XTab.readPlain()
    print "-- restored plaintext:", plain3
    assert(plain3==ciph2)

    print "FINAL:\n", printCipherText(ciph3)
    print "-- check:\n", printCipherText("777193862200032042396003829683146080607178016736060606463536069686740369681890014021906662606660863160549")

    t2Tab = DisruptedTranspositionTable(KX2)
    t2Tab.loadCipher(ciph3)
    ciph2 = t2Tab.readPlain()
    t1Tab = TranspositionTable(KX1)
    t1Tab.loadCipher(ciph2)
    ciph1 = t1Tab.readPlain()
    CB = CheckerBoard(cbKey)
    plain = CB.decrypt(ciph1)
    print "-- fully restored plaintext:", plain

# TODO: Create GUI
