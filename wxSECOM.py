#!/usr/bin/env python
# -*- coding:utf8 -*-
#
#
# wxSECOM - GUI-implementation of the SECOM cipher
#
# File:		wxSECOM.py
# Desc:		The wxWidgets-based GUI for SECOM.
#			Uses the framework from _wxSECOM.py.
#
# Version:	2010 Nov 01
#
# Usage: python wxSECOM.py
#
# Copyright (c) 2010 Jan D. Behrens, <zykure@web.de>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA


from _SECOM import *
import _wxSECOM
import wx


COLOR_TEXT_INPUT 			= wx.Colour( 247, 255, 247 )
COLOR_TEXT_OUTPUT 			= wx.Colour( 255, 247, 247 )
COLOR_KEY_SHORT         	= wx.Colour( 255, 195, 195 )
COLOR_KEY_OKAY          	= wx.Colour( 255, 255, 255 )

COLOR_ENTROPY_PERFECT 		= wx.Colour( 223, 255, 223 )
COLOR_ENTROPY_VERYGOOD  	= wx.Colour( 239, 255, 223 )
COLOR_ENTROPY_GOOD    		= wx.Colour( 255, 255, 223 )
COLOR_ENTROPY_OKAY    		= wx.Colour( 255, 239, 223 )
COLOR_ENTROPY_BAD     		= wx.Colour( 255, 223, 223 )

COLOR_CELL_EMPTY			= wx.Colour( 255, 255, 255 )
COLOR_CELL_CB_TOP			= wx.Colour( 255, 255, 223 )
COLOR_CELL_CB_NORMAL		= wx.Colour( 255, 239, 239 )
COLOR_CELL_TRANS1_NORMAL	= wx.Colour( 239, 255, 239 )
COLOR_CELL_TRANS2_NORMAL	= wx.Colour( 239, 239, 255 )
COLOR_CELL_TRANS2_TRIANG	= wx.Colour( 223, 223, 255 )

STATUS_FIELD_MODE			= 0
STATUS_FIELD_PLAINTEXT		= 1
STATUS_FIELD_CIPHERTEXT		= 2
STATUS_FIELD_KEY			= 3


CHECKERBOARD_CHARSETS	= [
							u"ES_TO_NI_A|BCDFGHJKLM|PQRUVWXYZ |1234567890",
							u"ESTONIA___|BCDFGHJKLM|PQRUVWXYZ |1234567890",
							u"INSERAT___|BCDFGHJKLM|OPQUVWXYZ |1234567890",
							u"SNEGOPA___|BCDFHIJKLM|QRTUVWXYZ |1234567890",
							u"ASINTOER__|BCDFGHJKLM|PQUVWXYZ /",
							u"СНЕГОПА___|БВДЁЖЗИЙКЛ|МHРТУФХЦЧШ|ЩЪЫЬЭЮЯ ./"
						  ]


def EntropyColoring(key):
	E = key.getEntropy()
	if  	E >= 0.9999:
		return COLOR_ENTROPY_PERFECT
	elif 	E >= 0.95:
		return COLOR_ENTROPY_VERYGOOD
	elif 	E >= 0.90:
		return COLOR_ENTROPY_GOOD
	elif 	E >= 0.75:
		return COLOR_ENTROPY_OKAY
	else:
		return COLOR_ENTROPY_BAD


class MainFrame(_wxSECOM.MainFrame):
	Keyphrase  		= None
	LeftKey    		= None
	RightKey   		= None
	CombinedKey 	= None
	ExpandedKey 	= None
	CBSourceKey 	= None
	CBKey			= None
	CheckerBoard	= None
	TMasterKey		= None
	TSourceKey		= None
	T1Key			= None
	T2Key			= None
	TKeyTab			= None
	T1Tab			= None
	T2Tab			= None
	plaintext   	= ""
	ciphertext  	= ""
	decrypt     	= False
	chainAddLength 	= 50
	transMinWidth 	= 10
	trans1Width		= 0
	trans2Width		= 0

	def __init__( self, parent ):
		_wxSECOM.MainFrame.__init__(self, parent)
		self.comboBoxCharacterSet.SetItems(CHECKERBOARD_CHARSETS)
		self.comboBoxCharacterSet.SetValue(CHECKERBOARD_CHARSETS[0])
		
		self.m_panel241.SetBackgroundColour(COLOR_ENTROPY_BAD)
		self.m_panel242.SetBackgroundColour(COLOR_ENTROPY_OKAY)
		self.m_panel243.SetBackgroundColour(COLOR_ENTROPY_GOOD)
		self.m_panel244.SetBackgroundColour(COLOR_ENTROPY_VERYGOOD)
		self.m_panel245.SetBackgroundColour(COLOR_ENTROPY_PERFECT)

		self.textCtrlKey.SetValue("MAKE NEW FRIENDS BUT KEEP THE OLD")
		self.textCtrlPlaintext.SetValue("RV TOMORROW AT 1400PM TO COMPLETE TRANSACTION USE DEADDROP AS USUAL")

		self.checkKeyInput(None)
		self.toggleEncDec(None)
		self.updateKeys(None)
		self.updateEncryption(None)


	def resizeGrid( self, object, numCols=-1, numRows=-1 ):
		if numCols > 0:
			if object.GetNumberCols() < numCols:
				object.AppendCols(numCols=numCols-object.GetNumberCols())
			else:
				object.DeleteCols(numCols=object.GetNumberCols()-numCols)
		if numRows > 0:
			if object.GetNumberRows() < numRows:
				object.AppendRows(numRows=numRows-object.GetNumberRows())
			else:
				object.DeleteRows(numRows=object.GetNumberRows()-numRows)

	def toggleEncDec( self, event ):
		if event:
			self.decrypt = (event.GetId() == _wxSECOM.BtnDecrypt)
			if		event.GetId() == _wxSECOM.BtnDecrypt and self.decrypt:
				self.updateDecryption(event)
			elif 	event.GetId() == _wxSECOM.BtnEncrypt and not self.decrypt:
				self.updateEncryption(event)

		self.toggleBtnEncrypt.SetValue(not self.decrypt)
		self.toggleBtnDecrypt.SetValue(self.decrypt)
		if self.decrypt:
			self.textCtrlPlaintext.SetBackgroundColour( COLOR_TEXT_OUTPUT )
			self.textCtrlPlaintext.SetEditable(False)
			self.textCtrlCiphertext.SetBackgroundColour( COLOR_TEXT_INPUT )
			self.textCtrlCiphertext.SetEditable(True)
			self.statusBar.SetStatusText("Decryption Mode", STATUS_FIELD_MODE)
		else:
			self.textCtrlCiphertext.SetBackgroundColour( COLOR_TEXT_OUTPUT )
			self.textCtrlCiphertext.SetEditable(False)
			self.textCtrlPlaintext.SetBackgroundColour( COLOR_TEXT_INPUT )
			self.textCtrlPlaintext.SetEditable(True)
			self.statusBar.SetStatusText("Encryption Mode", STATUS_FIELD_MODE)

	def setFocus( self, event ):
		if event.GetId() == _wxSECOM.Panel1:
			if not self.decrypt:
				self.textCtrlPlaintext.SetFocus()
				length = len(self.textCtrlPlaintext.GetValue())
				self.textCtrlPlaintext.SetSelection(length, length)
			else:
				self.textCtrlCiphertext.SetFocus()
				length = len(self.textCtrlCiphertext.GetValue())
				self.textCtrlCiphertext.SetSelection(length, length)

	def updateKeys( self, event ):
		keystring = self.textCtrlKey.GetValue()

		self.Keyphrase = Key()
		self.Keyphrase.fromString(keystring)
		if( len(self.Keyphrase) < 20 ):
			return False

		self.updateLeftRightKey(event)
		self.updateTransp(event)
		self.updateEncryption(event)
		self.updateDecryption(event)

	def updateLeftRightKey( self, event ):
		self.LeftKey = self.Keyphrase.subKey(0, 10)
		self.LeftKey.reduceAlpha()
		self.RightKey = self.Keyphrase.subKey(10, 10)
		self.RightKey.reduceAlpha()
		
		self.textCtrlLeftKey.SetValue( str(self.LeftKey) )
		self.textCtrlRightKey.SetValue( str(self.RightKey) )
		self.textCtrlLeftKey.SetBackgroundColour( EntropyColoring(self.LeftKey) )
		self.textCtrlRightKey.SetBackgroundColour( EntropyColoring(self.RightKey) )

	def updateCombinedKey( self, event ):
		self.CombinedKey = Key(self.LeftKey)
		self.CombinedKey.add(self.RightKey)

		self.textCtrlCombinedKey.SetValue( str(self.CombinedKey) )
		self.textCtrlCombinedKey.SetBackgroundColour( EntropyColoring(self.CombinedKey) )

	def updateExpandedKey( self, event ):
		self.chainAddLength = self.sliderChainAddLength.GetValue()
		self.staticTextChainAddLength.SetLabel( str(self.chainAddLength) )

		self.ExpandedKey = Key(self.CombinedKey)
		self.ExpandedKey.chainAdd(self.chainAddLength)
		self.ExpandedKey = self.ExpandedKey.subKey(10, self.chainAddLength)

		self.textCtrlExpandedKey.SetValue( str(self.ExpandedKey) )
		self.textCtrlExpandedKey.SetSelection(len(self.ExpandedKey), len(self.ExpandedKey))
		self.textCtrlExpandedKey.SetBackgroundColour( EntropyColoring(self.ExpandedKey) )
		
		self.updateTranspMinWidth( event )

	def updateCBSourceKey( self, event ):
		self.CBSourceKey = self.ExpandedKey.subKey(len(self.ExpandedKey)-10, 10)

		self.textCtrlCBSourceKey.SetValue( str(self.CBSourceKey) )
		self.textCtrlCBSourceKey.SetBackgroundColour( EntropyColoring(self.CBSourceKey) )

	def updateCBFinalKey( self, event ):
		self.CBKey = Key(self.CBSourceKey)
		self.CBKey.reduceNum()

		self.textCtrlCBFinalKey.SetValue( str(self.CBKey) )
		self.textCtrlCBFinalKey.SetBackgroundColour( EntropyColoring(self.CBKey) )

	def updateTranspMasterKey( self, event ):
		self.updateCheckerboard(event)

		self.TMasterKey = Key(self.RightKey)
		self.TMasterKey.add(self.CBKey)

		self.textCtrlTMasterKey.SetValue( str(self.TMasterKey) )
		self.textCtrlTMasterKey.SetBackgroundColour( EntropyColoring(self.TMasterKey) )

	def updateTranspSourceKey( self, event ):
		self.TKeyTab = TranspositionTable(self.TMasterKey)
		self.TKeyTab.loadPlain(self.ExpandedKey)
		self.TSourceKey = Key(self.TKeyTab.readCipher(getString=False))

		self.textCtrlTSourceKey.SetValue( str(self.TSourceKey) )
		self.textCtrlTSourceKey.SetSelection(len(self.TSourceKey), len(self.TSourceKey))
		self.textCtrlTSourceKey.SetBackgroundColour( EntropyColoring(self.TSourceKey) )

	def updateTranspMinWidth( self, event ):
		self.updateTransp(event)

		self.updateEncryption(event)
		self.updateDecryption(event)

	def updateTransp( self, event ):
		self.T1Tab = None
		self.T2Tab = None
		self.updateTranspKeys(event)

	def updateTranspKeys( self, event ):
		self.TransMinWidth = self.sliderTransMinWidth.GetValue()
		while True:
			self.trans1Width, self.trans2Width = getTranspositionDimensions(self.ExpandedKey, \
					minWidth=self.TransMinWidth)
			if len(self.ExpandedKey) >= self.trans1Width+self.trans2Width:
				break
			self.TransMinWidth -= 1

		self.sliderTransMinWidth.SetValue( self.TransMinWidth )
		self.staticTextTransMinWidth.SetLabel( str(self.TransMinWidth) )
		self.staticTextT1Width.SetLabel( str(self.trans1Width) )
		self.staticTextT2Width.SetLabel( str(self.trans2Width) )

		self.T1Key = self.TSourceKey.subKey(0, self.trans1Width)
		self.T2Key = self.TSourceKey.subKey(self.trans1Width, self.trans2Width)

		assert(len(self.T1Key) == self.trans1Width)
		assert(len(self.T2Key) == self.trans2Width)

		self.textCtrlT1Key.SetValue( str(self.T1Key) )
		self.textCtrlT2Key.SetValue( str(self.T2Key) )
		self.textCtrlT1Key.SetBackgroundColour( EntropyColoring(self.T1Key) )
		self.textCtrlT2Key.SetBackgroundColour( EntropyColoring(self.T2Key) )

	def updateCheckerboard( self, event ):
		if not self.CBKey:
			return False

		self.resizeGrid(self.gridCheckerboard, numCols=len(self.CBKey))
		for i in range(len(self.CBKey)):
			self.gridCheckerboard.SetColLabelValue(i, str(self.CBKey[i]))
			self.gridCheckerboard.SetColSize(i, 50)

		self.charset = self.comboBoxCharacterSet.GetValue().upper()
		self.charset = "".join(self.charset.split("|"))
		self.CheckerBoard = CheckerBoard(self.CBKey, charset=self.charset)

		board = self.CheckerBoard.getBoard()
		self.resizeGrid(self.gridCheckerboard, numRows=len(board))
		self.gridCheckerboard.ClearGrid()

		for r in range(len(board)):
			if r == 0:
				self.gridCheckerboard.SetRowLabelValue(r, u"—")
			else:
				self.gridCheckerboard.SetRowLabelValue(r, str(board[r][0]))
			for c in range(len(self.CBKey)):
				if board[r][1][c] == "_":
					self.gridCheckerboard.SetCellBackgroundColour(r, c, COLOR_CELL_EMPTY)
					continue
				self.gridCheckerboard.SetCellValue(r, c, board[r][1][c])
				if r == 0:
					self.gridCheckerboard.SetCellBackgroundColour(r, c, COLOR_CELL_CB_TOP)
				else:
					self.gridCheckerboard.SetCellBackgroundColour(r, c, COLOR_CELL_CB_NORMAL)

		self.updateCheckerboardTest(event)
		self.updateEncryption(event)
		self.updateDecryption(event)

	def updateTransp1( self, event ):
		if not self.T1Key:
			return False

		if self.T1Tab == None:
			self.T1Tab = TranspositionTable(self.T1Key)

		self.resizeGrid(self.gridTransp1, numCols=self.trans1Width)

		for i in range(self.trans1Width):
			self.gridTransp1.SetColLabelValue(i, str(self.T1Key[i]))
			self.gridTransp1.SetColSize(i, 40)

		if len(self.T1Tab) > 0:
			rows = self.T1Tab.getRows()
			self.resizeGrid(self.gridTransp1, numRows=len(rows))

			for r in range(len(rows)):
				for c in range(self.trans1Width):
					if c < len(rows[r]):
						if rows[r][c] != None:
							self.gridTransp1.SetCellValue(r, c, str(rows[r][c]))
							self.gridTransp1.SetCellBackgroundColour(r, c, COLOR_CELL_TRANS1_NORMAL)
							continue
					self.gridTransp1.SetCellValue(r, c, u"")
					self.gridTransp1.SetCellBackgroundColour(r, c, COLOR_CELL_EMPTY)

	def updateTransp2( self, event ):
		if not self.T2Key:
			return False

		if self.T2Tab == None:
			self.T2Tab = DisruptedTranspositionTable(self.T2Key)

		self.resizeGrid(self.gridTransp2, numCols=self.trans2Width)

		for i in range(self.trans2Width):
			self.gridTransp2.SetColLabelValue(i, str(self.T2Key[i]))
			self.gridTransp2.SetColSize(i, 40)

		if len(self.T2Tab) > 0:
			rows = self.T2Tab.getRows()
			self.resizeGrid(self.gridTransp2, numRows=len(rows))

			for r in range(len(rows)):
				for c in range(self.trans2Width):
					if c < len(rows[r][1]):
						if rows[r][1][c] != None:
							self.gridTransp2.SetCellValue(r, c, str(rows[r][1][c]))
							if c < rows[r][0]:
								self.gridTransp2.SetCellBackgroundColour(r, c, COLOR_CELL_TRANS2_NORMAL)
							else:
								self.gridTransp2.SetCellBackgroundColour(r, c, COLOR_CELL_TRANS2_TRIANG)
							continue
					self.gridTransp2.SetCellValue(r, c, u"")
					self.gridTransp2.SetCellBackgroundColour(r, c, COLOR_CELL_EMPTY)


	def updateCheckerboardTest( self, event ):
		if not self.CheckerBoard:
			return False

		plain = self.textCtrlCheckerboardTestInput.GetValue()
		cipher = self.CheckerBoard.encrypt(plain)
		self.textCtrlCheckerboardTestOutput.SetValue(cipher)

		self.staticTextCheckerboardTestInputLength.SetLabel( str(len(plain)) )
		self.staticTextCheckerboardTestOutputLength.SetLabel( str(len(cipher)) )

	def updateEncryption( self, event ):
		if self.decrypt:
			return False
		if self.CheckerBoard == None or self.T1Tab == None or self.T2Tab == None:
			return False

		self.plaintext = u""
		for x in self.textCtrlPlaintext.GetValue().upper():
			if not x in self.charset:
				x = u" "
			self.plaintext += x

		self.statusBar.SetStatusText(u"Plaintext: " + str(len(self.plaintext)) \
				+ u" chars", STATUS_FIELD_PLAINTEXT)

		ciph1 = self.CheckerBoard.encrypt(self.plaintext)

		self.T1Tab.loadPlain(ciph1)
		ciph2 = self.T1Tab.readCipher()
		self.updateTransp1( event )

		self.T2Tab.loadPlain(ciph2)
		self.ciphertext = self.T2Tab.readCipher()
		self.updateTransp2( event )

		self.statusBar.SetStatusText(u"Ciphertext: " + str(len(self.ciphertext)) \
				+ u" digits", STATUS_FIELD_CIPHERTEXT)

		self.ciphertext = printCipherText(self.ciphertext, blocksPerLine=-1).strip()
		self.textCtrlCiphertext.SetValue( self.ciphertext )

	def updateDecryption( self, event ):
		if not self.decrypt:
			return False
		if self.CheckerBoard == None or self.T1Tab == None or self.T2Tab == None:
			return False

		self.ciphertext = u""
		for x in self.textCtrlCiphertext.GetValue():
			if x in "0123456789":
				self.ciphertext += x

		self.statusBar.SetStatusText(u"Ciphertext: " + str(len(self.ciphertext)) \
				+ u" digits", STATUS_FIELD_CIPHERTEXT)

		self.T2Tab.loadCipher(self.ciphertext)
		ciph2 = self.T2Tab.readPlain()
		self.updateTransp2( event )

		self.T1Tab.loadCipher(ciph2)
		ciph1 = self.T1Tab.readPlain()
		self.updateTransp1( event )

		self.plaintext = self.CheckerBoard.decrypt(ciph1)

		self.textCtrlPlaintext.SetValue( self.plaintext )
		self.statusBar.SetStatusText(u"Plaintext: " + str(len(self.plaintext)) \
				+ u" chars", STATUS_FIELD_PLAINTEXT)

	def checkKeyInput( self, event ):
		keystring = self.textCtrlKey.GetValue()
		count = 0
		if keystring:
			for x in keystring:
				if x.upper() in "ABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890":
					count += 1
		if count < 20:
			self.textCtrlKey.SetBackgroundColour( COLOR_KEY_SHORT )
			self.statusBar.SetStatusText(u"Key: Short (" + str(count) + u" chars)", STATUS_FIELD_KEY)
		else:
			self.textCtrlKey.SetBackgroundColour( COLOR_KEY_OKAY )
			self.statusBar.SetStatusText(u"Key: Okay (" + str(count) + u" chars)", STATUS_FIELD_KEY)

	def updatePlaintext( self, event ):
		self.updateEncryption(event)
		self.textCtrlPlaintext.SetValue( self.plaintext )

	def updateCiphertext( self, event ):
		self.textCtrlCiphertext.SetValue( self.ciphertext )


app = wx.App()
window = MainFrame(None)
window.Show(True)
app.MainLoop()
